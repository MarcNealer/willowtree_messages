from rest_framework import routers
from .views import AccountManagementViewset, AccountInvitesViewset, AccountUserViewset


router = routers.DefaultRouter()

router.register(r'account/management', AccountManagementViewset, base_name='account_management')
router.register(r'account/users', AccountUserViewset, base_name='account_users')
router.register(r'account/invites', AccountInvitesViewset, base_name='supply_res_inv')

urlpatterns = [] + router.urls