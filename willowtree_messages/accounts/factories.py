import factory.fuzzy
from factory.django import DjangoModelFactory
from .models import Accounts
from django.contrib.auth.models import User

class UserFactory(DjangoModelFactory):
    username = factory.Sequence(lambda n: "testuser%d" % n)
    email = factory.Sequence(lambda n: "test%d@test.com" % n)
    password =  factory.fuzzy.FuzzyText(length=12, chars='abcdefghijzu1234567890')
    first_name = factory.Faker('name')
    last_name = factory.Faker('name')

    class Meta:
        model = User
        django_get_or_create = ('username',)

class AccountFactory(DjangoModelFactory):
    name = factory.Sequence(lambda n: "Account%d" % n)
    contact = factory.Faker('name')
    phone = factory.Sequence(lambda n: "0066 11122 000%d" % n)
    email = factory.Sequence(lambda n: "test%d@test.com" % n)
    complaint_email = factory.Sequence(lambda n: "complaint%d@test.com" % n)

    class Meta:
        model = Accounts
        django_get_or_create = ('name',)




