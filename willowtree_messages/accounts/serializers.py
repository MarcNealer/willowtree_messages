from rest_framework import serializers
from .models import Accounts, AccountUsers, Invites
from django.contrib.auth.models import User


class AccountsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Accounts
        fields = "__all__"


class AccountUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountUsers
        fields = "__all__"

class InvitesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invites
        fields = "__all__"

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "full_name")

    full_name = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        if obj.last_name and obj.first_name:
            return "{} {}".format(obj.first_name, obj.last_name)
        else:
            return obj.username


