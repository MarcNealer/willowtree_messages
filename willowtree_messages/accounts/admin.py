from django.contrib import admin
from .models import Accounts, AccountUsers, Invites
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site

# Register your models here.

admin.site.site_header = 'Willowtree Messages Admin' # Main header text
admin.site.site_title = "Messages Admin" # Page/tab text in browser
admin.site.index_title = 'Message Admin' # sub header text

admin.site.unregister(Group)
admin.site.unregister(Site)


class AccountsAdmin(admin.ModelAdmin):
    list_display = ('name', 'contact', 'updated_on')
    list_editable = ('contact',)
    list_filter = ('name', 'contact')
    search_fields = ['name', 'contact']
    fieldsets = (
        ("Main", {'fields': ('name', ('contact','phone', 'email'))}),
        ("settings", {'fields':(('complaint_email', "subscription"), ('send_to_pupils', 'send_to_parents')),
                      "description":"Account system settings"},)
    )


    actions = ['print_ids']
    date_hierarchy = "updated_on"


    def print_ids(self, request, queryset):
        print(list(queryset.values_list('id', flat=True)))
    print_ids.short_description = "print ids"

admin.site.register(Accounts, AccountsAdmin)
admin.site.register(AccountUsers)
admin.site.register(Invites)
