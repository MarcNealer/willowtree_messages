from django.db import models
from django.contrib.auth.models import User


import random
# Create your models here.

STATUS_CODES = (('A', 'Admin'), ('H', 'Head'), ('T', 'Teacher'))


class Updated(models.Model):
    updated_by = models.ForeignKey(User, blank=True, null=True,
                                   related_name="%(class)s_updatedby", on_delete=models.SET_NULL)
    updated_on = models.DateTimeField(auto_now=True, blank=True, null=True)
    active = models.BooleanField(default=True)
    created = models.DateTimeField(editable=False, auto_now_add=True)

    class Meta:
        abstract = True


class Accounts(Updated):
    name = models.CharField(max_length=200)
    contact = models.CharField(max_length=200)
    phone = models.CharField(max_length=50)
    email = models.EmailField()
    complaint_email = models.EmailField()
    subscription = models.CharField(max_length=20, default='free')
    send_to_pupils = models.BooleanField(default=True)
    send_to_parents = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class AccountUsers(Updated):

    user = models.ForeignKey(User, related_name="account_user", on_delete=models.CASCADE)
    account = models.ForeignKey(Accounts, related_name="user_account", on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=STATUS_CODES)



class Invites(Updated):
    account = models.ForeignKey(Accounts, on_delete=models.CASCADE)
    email = models.CharField(max_length=400)
    code = models.CharField(max_length=30, blank=True, null=True)
    type = models.CharField(max_length=1, choices=STATUS_CODES)

    def __str__(self):
        return self.code

    def save(self, *args, **kwargs):
        if not self.pk:
            choices = 'abcdefghijklmnopqrstuvwxyz1234567890'
            self.code = ''.join([random.choice(choices) for _ in range(20)])
        super().save(*args, **kwargs)  # Call the "real" save() method.

