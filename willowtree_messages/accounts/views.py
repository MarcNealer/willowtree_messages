from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from .permissions import Permissions
from .serializers import AccountUserSerializer, AccountsSerializer, InvitesSerializer
from .models import Invites, Accounts, AccountUsers, User
# Create your views here.


class AccountManagementViewset(viewsets.ViewSet):

    def create(self, request):
        """
        creates a new account
        :param request:
        :return:
        """
        perm = Permissions(request)
        if perm.status():
            return Response('Permission Failed. An account cannot be created by a registered user',
                            status=status.HTTP_400_BAD_REQUEST)
        ser = AccountsSerializer(data=request.data)
        if ser.is_valid():
            ser.save()
            Invites.objects.create(account=ser.instance, type="A", email=ser.instance.email)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self,request):
        """
        lists accounts you have access to
        :param request:
        :return:
        """
        perm = Permissions(request)
        account = perm.account()
        if not account:
            return Response('No Account found', status=status.HTTP_403_FORBIDDEN)
        recs = [account]
        ser = AccountsSerializer(recs, many=True)
        return Response(ser.data)

    def update(self, request, pk):
        """
        updates a given account
        :param request:
        :param pk:
        :return:
        """
        perm = Permissions(request)
        if not perm.is_admin():
            return Response('Not Admin User', status=status.HTTP_403_FORBIDDEN)
        ser = AccountsSerializer(perm.account(), data=request.data)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountInvitesViewset(viewsets.ViewSet):

    def create(self, request):
        """
        Create a New Invite code for an Account and send a message
        :param request:
        :return:
        """
        perm = Permissions(request)
        if not perm.is_admin():
            return Response('Only admin users can create Invites', status=status.HTTP_403_FORBIDDEN)
        if not 'type' in request.data:
            return Response('Type of User is required', status=status.HTTP_400_BAD_REQUEST)
        if not 'email' in request.data:
            return Response('Email address of the user is required', status=status.HTTP_400_BAD_REQUEST)
        Invites.objects.create(account=perm.account(),
                               type=request.data['type'], email=request.data['email'],
                               updated_by=request.user)
        return Response('Invite created and sent')

    @action(detail=False)
    def check(self, request):
        """
        checks to see if an invite code is valid
        :param request:
        :return:
        """
        if 'code' not in request.GET:
            return Response('invite code not sent', status=status.HTTP_400_BAD_REQUEST)
        rec = Invites.objects.filter(active=True, code= request.GET['code'])
        if rec.exists():
            return Response('Valid Code')
        else:
            return Response('Invalid Code', status=status.HTTP_400_BAD_REQUEST)
        pass

    @action(detail=False)
    def account_invites(self, request):
        """
        returns a list of emails with active invites
        :param request:
        :param pk:
        :return:
        """
        perm = Permissions(request)
        if not perm.is_admin():
            return Response('Admin Only', status=status.HTTP_403_FORBIDDEN)
        invites = Invites.objects.filter(active=True, account=perm.account())
        ser = InvitesSerializer(invites, many=True)
        return Response(ser.data)


class AccountUserViewset(viewsets.ViewSet):

    def list(self, request):
        """
        lists users for an account and their status
        :param request:
        :return:
        """
        perm = Permissions(request)
        if not perm.status():
            return Response('No account listed', status=status.HTTP_403_FORBIDDEN)
        recs = AccountUsers.objects.filter(active=True, account=perm.account())
        ser = AccountUserSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def create_user(self, request):
        """
        Creates a new user and adds them to an account, after checking their invite code
        :return:
        """
        perm = Permissions(request)
        if perm.status():
            return Response('Your a registed user. You cannot create an account', status=status.HTTP_403_FORBIDDEN)
        if 'code' not in request.data:
            return Response('Invite code MUST be sent', status=status.HTTP_403_FORBIDDEN)
        if not 'username' in request.data or 'password' not in request.data:
            return Response('Username and password not sent', status=status.HTTP_400_BAD_REQUEST)
        invite = Invites.objects.filter(active=True, code=request.data['code'])
        if not invite.exists():
            return Response('Invite code invalid', status=status.HTTP_400_BAD_REQUEST)
        try:
            new_user = User.objects.create(username=request.data['username'],
                                           email=invite[0].email, password=request.data['password'])
            if 'first_name' in request.data:
                new_user.first_name = request.data['first_name']
                new_user.save()
            if 'last_name' in request.data:
                new_user.last_name = request.data['last_name']
                new_user.save()
        except Exception as e:
            return Response('Failed to create User:{}'.format(e), status=status.HTTP_400_BAD_REQUEST)
        AccountUsers.objects.create(account=invite[0].account, user=new_user, type=invite[0].type)
        return Response('new user created')

    @action(detail=False)
    def get_status(self, request):
        """
        returns a users status in their account
        :param request:
        :return:
        """
        perm = Permissions(request)
        if not perm.status():
            return Response('Failed. No registered User', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'status': perm.status()})

    @action(detail=False)
    def update_user_status(self, request):
        """
        updates a users status in a given account
        :param request:
        :return:
        """
        perm = Permissions(request)
        if not perm.is_admin():
            return Response('Admin Only', status=status.HTTP_403_FORBIDDEN)
        if 'user' not in request.data or 'type' not in request.data:
            return Response('User and Type not specififed', status=status.HTTP_400_BAD_REQUEST)
        rec = AccountUsers.objects.filter(user__id=request.data['user'], account=perm.account(), active=True)
        if not rec.exists():
            return Response('Account User not found', status=status.HTTP_400_BAD_REQUEST)
        rec = rec[0]
        rec.type = request.data['type']
        rec.updated_by = request.user
        rec.save()
        ser = AccountUserSerializer(rec)
        return Response(ser.data)

    def destroy(self, request, pk):
        """
        removes a user from an account
        :param request:
        :param pk:
        :return:
        """
        perm = Permissions(request)
        if not perm.is_admin():
            return Response('Admin Only', status=status.HTTP_403_FORBIDDEN)
        rec = AccountUsers.objects.filter(active=True, user__id=pk, account=perm.account())
        if not rec.exists():
            return Response('no user found', status=status.HTTP_400_BAD_REQUEST)
        rec= rec[0]
        rec.active=False
        rec.updated_by=request.user
        rec.save()
        return Response('User Removed', status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk):
        perm = Permissions(request)
        if not perm.status():
            return Response('Not a registered User', status=status.HTTP_403_FORBIDDEN)
        recs = AccountUsers.objects.filter(user__id=pk, account=perm.account())
        if not recs.exists():
            return Response('User not found for this account', status=status.HTTP_400_BAD_REQUEST)

