from django.db.models.signals import post_save
from .models import Invites
from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string


@receiver(post_save, sender=Invites)
def send_invite_email(sender, instance, **kwargs):
    print('Send email function activated')
    from django.contrib.sites.models import Site
    current_site = Site.objects.get_current()
    message = render_to_string("emails/invite_message.txt", {'rec': instance, 'site': current_site})
    send_mail('Willowtreee Messages Invite',
              message,
              settings.DEFAULT_FROM_EMAIL,
              [instance.email, ])

