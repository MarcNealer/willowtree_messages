from .models import AccountUsers
from rest_framework.response import Response
from rest_framework import status


class Permissions:

    def __init__(self, request, level=None, account=None):
        self.request = request
        self.account_status = None
        self.subscription_obj = None
        if request.user.is_authenticated:
            account_status = AccountUsers.objects.filter(user=request.user)
            if account_status.exists():
                self.account_status = account_status[0]
                if account_status.count() > 1:
                    print("User has multiple account records. Please ammend: {}".format(request.user.username))
        if level:
            self.__check_level__(level)

        if account:
            self.__check_account__(account)

    def __check_level__(self, level):
        if "admin" in level:
            if not self.is_admin():
                return Response('Permission failed', status=status.HTTP_403_FORBIDDEN)
        elif "head" in level:
            if not self.is_head():
                return Response('Permission Failed', status=status.HTTP_403_FORBIDDEN)
        elif "teacher" in level:
            if not self.is_teacher():
                return Response('Permission Failed', status=status.HTTP_403_FORBIDDEN)
        elif "auth" in level:
            if not self.status():
                return Response('Permission Failed', status=status.HTTP_403_FORBIDDEN)
        return True

    def __check_account__(self, account):
        if self.account() != account:
            return Response('Permission Failed', status=status.HTTP_403_FORBIDDEN)

    def status(self):
        if not self.account_status:
            return False
        else:
            return self.account_status.status

    def account(self):
        if not self.account_status:
            return False
        else:
            return self.account_status.account

    def is_admin(self):
        if self.account_status.status == "A":
            return True
        else:
            return False

    def is_head(self):
        if self.account_status.status in ["H", "A"]:
            return True
        else:
            return False

    def is_teacher(self):
        if self.account_status.status in ["T", "H", "A"]:
            return True
        else:
            return False
    def subscription(self):
        if not self.subscription_obj:
            self.subscription_obj = Subscription(self)
        return self.subscription_obj

class Subscription():

    def __init__(self, perm):
        self.permissions = perm
        self.account = self.permissions.account()
        if not self.account:
            raise Exception('failed to get account details')
        self.subscription =self.account.subscription
        self.subscriptions = {'free':{'pupils':100, 'parents':1, 'size':200},
            'level1': {'pupils':500, 'parents':2, 'size':350},
            'level2': {'pupils':1000, 'parents': 3, 'size':350},
            'level3': {'pupils':10000, 'parents':6 , 'size':350}}

    def allowed_pupils(self):
        return self.subscriptions[self.subscription]['pupils']

    def allowed_parents(self):
        return self.subscriptions[self.subscription]['parents']

    def allowed_message_size(self):
        return self.subscriptions[self.subscription]['size']