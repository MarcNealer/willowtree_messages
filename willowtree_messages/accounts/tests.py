from django.test import TestCase
from rest_framework.test import APIClient
from .factories import AccountFactory, UserFactory
from .models import Invites
# Create your tests here.


class AccountsTest(TestCase):
    def setUp(self):
        self.cli = APIClient()
        self.test_account_data = {'name':'test_account', 'email':'test@test.com',
                                  'phone':'009332122', 'complaint_email':'complaints@test.com'}

    def __create_account__(self):
        self.account = AccountFactory()

    def __add_admin__(self):
        user = UserFactory()
        self.account.admin.add(user)
        self.account.save()

    def __add_head__(self):
        user = UserFactory()
        self.account.heads.add(user)
        self.account.save()

    def __add_teacher__(self):
        user =UserFactory()
        self.account.teachers.add(user)
        self.account.save()

    def __add_invite__(self):
        return Invites.objects.create(account=self.account)

    def test_create_account(self):
        resp = self.cli.post('/api/accounts/', self.test_account_data)
        self.assertContains(resp,'test_account')
        data = resp.json()
        for key, item in data.items():
            self.assertEquals(self.test_account_data[key], data[key])

    def test_new_user(self):
        self.__create_account__()
        code = self.__add_invite__()
        payload = {'invite':code.code, 'username':'unittest1',
                   'email':'test1@test.com', 'password':'testtest'}
        resp = self.cli.post('/api/accounts/%d/newuser/' % self.account.id, payload)
        self.assertContains(resp, 'unittest1')
        self.assertContains(resp, 'test1@test.com')
        self.assertNotContains(resp, 'testtest')

    def test_bad_invite(self):
        self.__create_account__()
        payload = {'invite': 'asddadasdasd', 'username':'unittest1',
                   'email':'test1@test.com', 'password':'testtest'}
        resp = self.cli.post('/api/accounts/%d/newuser/' % self.account.id, payload)
        self.assertEquals(resp.status_code, 401)

    def test_get_account(self):
        self.__create_account__()
        admin = self.__add_admin__()
        resp = self.cli.get('/api/accounts/')
        self.assertContains(resp, 'test_account')

    def test_change_user_status(self):
        self.__create_account__()
        admin = self.__add_admin__()
        teacher = self.__add_teacher__()
        self.cli.force_authenticate(admin)
        payload = {'user': teacher.id, 'status': 'head'}
        resp = self.cli.post('/api/account/%d/changeuser/' % self.account.id, payload)
        self.assertContains(resp, teacher.username)
        self.assertContains(resp, 'head')

    def test_head_change_status(self):
        self.__create_account__()
        admin = self.__add_head__()
        teacher = self.__add_teacher__()
        self.cli.force_authenticate(admin)
        payload = {'user': teacher.id, 'status': 'head'}
        resp = self.cli.post('/api/account/%d/changeuser/' % self.account.id, payload)
        self.assertEquals(resp.status_code, 401)


    def test_unauth_account_change(self):
        self.__create_account__()
        head = self.__add_head__()
        self.cli.force_authenticate(head)
        resp = self.cli.put('/api/accounts/%d/' % self.account.id, {'name': "updated_name"})
        self.assertEquals(resp.status_code, 401)

    def test_account_change(self):
        self.__create_account__()
        admin = self.__add_admin__()
        self.cli.force_authenticate(admin)
        resp = self.cli.put('/api/accounts/%d/' % self.account.id, {'name': "updated_name"})
        self.assertContains(resp, "updated_name")

    def test_failed_to_remove_user(self):
        self.__create_account__()
        head = self.__add_head__()
        teacher = self.__add_teacher__()
        self.cli.force_authenticate(head)
        resp = self.cli.post('/api/account/%d/remove_user/' % self.account.id, {'user': teacher.id})
        self.assertEquals(resp.status_code, 401)

    def test_remove_user(self):
        self.__create_account__()
        admin = self.__add_admin__()
        teacher = self.__add_teacher__()
        self.cli.force_authenticate(admin)
        resp = self.cli.post('/api/account/%d/remove_user/' % self.account.id, {'user': teacher.id})
        self.assertEquals(resp.status_code, 204)

    def test_remove_me(self):
        self.__create_account__()
        admin = self.__add_admin__()
        self.cli.force_authenticate(admin)
        resp = self.cli.post('/api/accounts/%d/remove_user/' % self.account.id, {'user':admin.id})
