from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from accounts.permissions import Permissions
from .serializers import MessageSerializer
from .models import *
import datetime

class MessageViewset(viewsets.ViewSet):

    def create(self, request):
        Permissions(request,"auth", request.data['account'])
        ser = MessageSerializer(data=request.data)
        if ser.is_valid():
            ser.save(updated_by=request.user, created_by=request.user, msg_from=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors)

    def update(self, request, pk):
        perm = Permissions(request, "auth")
        if perm.is_admin():
            recs = Message.objects.filter(id=pk, account=perm.account())
        else:
            recs = Message.objects.filter(id=pk, updated_by=request.user,
                                          account=perm.account(), approved=False)
        if not recs.exists():
            return Response('Message not found or can no longer be edited', status=status.HTTP_400_BAD_REQUEST)
        ser = MessageSerializer(recs[0], data=request.data, partial=True)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        perm = Permissions(request, "auth")
        if perm.is_admin():
            recs = Message.objects.filter(id=pk, account=perm.account())
        else:
            recs = Message.objects.filter(id=pk, updated_by=request.user,
                                          account=perm.account(), approved=False)
        if not recs.exists():
            return Response('Message not found or can no longer be edited', status=status.HTTP_400_BAD_REQUEST)
        rec = recs[0]
        rec.updated_by = request.user
        rec.active = False
        rec.save()
        return Response('', status=status.HTTP_204_NO_CONTENT)

    def list(self, request):
        perm = Permissions(request, "auth")
        if perm.is_head():
            recs = Message.objects.filter(active=True, msg_sent=False)
        else:
            recs = Message.objects.filter(active=True, msg_sent=False, created_by=request.user)
        ser = MessageSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def approved(self, request):
        perm = Permissions(request, "auth")
        if perm.is_head():
            recs = Message.objects.filter(active=True, end_date__gte=datetime.datetime.today(),
                                          approved=True)
        else:
            recs = Message.objects.filter(active=True, end_date__gte=datetime.datetime.today(),
                                          created_by=request.user, approved=True)
        ser = MessageSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def unapproved(self, request):
        perm = Permissions(request, "auth")
        if perm.is_head():
            recs = Message.objects.filter(active=True, msg_sent=False, approved=False, account=perm.account())
        else:
            recs = Message.objects.filter(active=True, msg_sent=False, created_by=request.user, approved=False, account=perm.account())
        ser = MessageSerializer(recs, many=True)
        return Response(ser.data)


    @action(detail=True)
    def for_pupil(self, request, pk):
        perm = Permissions(request, "auth")
        recs = Message.objects.filter(active=True, approved=True, msg_to__groupmembership__pupil__id=pk,
                                      account=perm.account())
        ser = MessageSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def sent_messages(self, request):
        perm = Permissions(request, "admin")
        recs = Message.objects.filter(Active=True, msg_sent=True).order_by('-id')[:100]
        ser = MessageSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def message_stats(self, request):
        perm = Permissions(request, "teacher")
        stats = {"approved":0, "unapproved": 0, "sent": 0}
        if perm.account_status.status in ["H", "A"]:
            stats['approved'] = Message.objects.filter(account=perm.account(), active=True,
                                                       approved=True, msg_sent=False).count()
            stats['unapproved'] = Message.objects.filter(account=perm.account(), active=True,
                                                         approved=False, msg_sent=False).count()
            stats['sent'] = Message.objects.filter(account=perm.account(), active=True, msg_sent=True).count()
        else:
            stats['approved'] = Message.objects.filter(account=perm.account(), active=True, created_by=request.user,
                                                       approved=True, msg_sent=False).count()
            stats['unapproved'] = Message.objects.filter(account=perm.account(), active=True, created_by=request.user,
                                                         approved=False, msg_sent=False).count()
            stats['sent'] = Message.objects.filter(account=perm.account(), active=True,
                                                   created_by=request.user, msg_sent=True).count()
        return Response(stats)

