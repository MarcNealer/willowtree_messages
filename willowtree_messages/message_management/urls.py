from rest_framework import routers
from .views import MessageViewset


router = routers.DefaultRouter()

router.register(r'messages', MessageViewset, base_name='messages')

urlpatterns = [] + router.urls