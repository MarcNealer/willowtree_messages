from django.db import models
from accounts.models import Updated, Accounts
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

class Message(Updated):
    account = models.ForeignKey(Accounts,  on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=200)
    type = models.CharField(max_length=100, default="homework")
    message = models.TextField()
    msg_from = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='msg_from')
    msg_to = models.ManyToManyField("pupils.PupilGroup", blank=True)
    msg_sent = models.BooleanField(default=False)
    approved = models.BooleanField(default=True)
    created_by = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title





