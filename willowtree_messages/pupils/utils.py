from .models import *
import copy


class GroupController:

    def __init__(self, group, build_tree=False):
        self.build_tree = build_tree
        self.group = group
        if self.group.parent:
            self.is_parent = True
        else:
            self.is_parent = False
        self.membership = []
        self.possible_membership = []
        self.parent = None
        self.subgroups = []
        if build_tree:
            self.__get_subgroups__(build_tree)

    def list_membership(self):
        if not self.membership:
            self.membership = set([x.pupil.id for x in GroupMembership.objects.filter(group=self.group,
                                                                                      active=True)])
        return self.membership

    def list_possible_membership(self):
        if self.group.parent:
            self.parent = GroupController(self.group.parent)
            membership = self.parent.list_membership()
            membership = membership - self.list_membership()
            return membership
        else:
            current = self.list_membership()
            all = set([x.id for x in Pupil.objects.filter(active=True, account=self.group.account)])
            return all-current

    def list_subgroups(self):
        if not self.subgroups:
            self.__get_subgroups__(build_tree=False)
        return [x.group for x in self.subgroups]

    def __get_subgroups__(self, build_tree):
        self.subgroups = [GroupController(x, build_tree=build_tree) for x in
                          PupilGroup.objects.filter(active=True, parent=self.group)]

    def list_subgroup_membership(self):
        data = []
        for group in self.subgroups:
            data.append({'group': group.group.id, 'name': group.group.name, 'membership': group.list_membership()})

    def __is_member__(self, pupil):
        if pupil in self.membership:
            return True
        else:
            return False

    def clear_group(self, request):
        if not self.subgroups:
            self.__get_subgroups__(build_tree=True)

        GroupMembership.objects.filter(group=self.group).update(active=False, updated_by=request.user)
        for group in self.subgroups:
            group.clear_group(request)
        return True

    def not_in_subset(self):
        if not self.subgroups:
            self.__get_subgroups__(build_tree=False)
        not_member = copy.deepcopy(self.membership)
        for group in self.subgroups:
            not_member = not_member - group.membership
        return not_member

    def move_group(self, new_group, request, tag=None):
        new_group = GroupController(new_group)
        parents = new_group.__list_parents__()
        new_recs = []
        for group in parents:
            for pupil in self.membership:
                if tag:
                    new_recs.append(GroupMembership(pupil=Pupil.objects.get(id=pupil), group=group, tag=tag))
                else:
                    new_recs.append(GroupMembership(pupil=Pupil.objects.get(id=pupil), group=group))
        GroupMembership.objects.bulk_create(new_recs)
        self.clear_group(request)
        return True

    def __list_parents__(self):
        if not self.group.parent:
            return []
        else:
            return [self.group.parent] + GroupController(self.group.parent).__list_parents__()

    def __add_member__(self, pupil, request, tag=None):
        GroupMembership.objects.create(pupil=pupil, group=self.group, updated_by=request.user)
        return True

    def add_member(self, pupil, request):
        if not self.__is_member__(pupil):
            self.__add_member__(pupil, request)
        for grp in self.__list_parents__():
            grp_obj = GroupController(grp)
            if not grp_obj.__is_member__(pupil):
                grp_obj.__add_member__(pupil, request)
        return True

    def remove_member(self, pupil, request):
        recs = GroupMembership.objects.filter(group=self.group, pupil=pupil, active=True)
        for rec in recs:
            rec.active = False
            rec.updated_by = request.user
            rec.save()
        self.__get_subgroups__(build_tree=False)
        for grp in self.subgroups:
            grp.remove_member(pupil, request)
        return True

    def all_groups(self):
        groups = []
        groups.append(self.group)
        for group in self.subgroups:
            groups = groups + group.all_groups()
        return groups
