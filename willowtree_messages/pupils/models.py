from django.db import models
from accounts.models import Updated
# Create your models here.


class Pupil(Updated):
    """
    The main Pupil record object. Holds only basic Pupil info
    """
    account = models.ForeignKey("accounts.Accounts", on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    status = models.CharField(max_length=10, default='current')

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

class Parent(Updated):
    """
    Parent Records connected to Pupils
    """
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name  = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField()
    pupil = models.ManyToManyField(Pupil, blank=True)
    account = models.ForeignKey("accounts.Accounts", on_delete=models.CASCADE)

    def __str__(self):
        return self.email

    class Meta:
        unique_together = ('account', 'email')


class PupilGroup(Updated):
    """
    PupilGroups are a tag name for a class or group that one or more pupils can
    belong to.

    """
    account = models.ForeignKey("accounts.Accounts", on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class PupilSets(Updated):
    """
    Pupil Sets are a group og PupilGroups where a given Pupil should
    only be in one of the given groups

    """
    account = models.ForeignKey("accounts.Accounts", on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    groups = models.ManyToManyField(PupilGroup, blank=True)

    class Meta:
        unique_together = ('account', 'name')

    def __str__(self):
        return self.name


class GroupMembership(Updated):
    """
    Linking Record assigning membership of a pupil in a group with a selected tag
    """
    pupil = models.ForeignKey(Pupil, on_delete=models.CASCADE)
    group = models.ForeignKey(PupilGroup, on_delete=models.CASCADE)

    def __str__(self):
        return self.group.name
