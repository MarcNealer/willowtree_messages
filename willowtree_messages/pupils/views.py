import csv
from django.db import transaction
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from accounts.permissions import Permissions
from .models import *
from .serializers import PupilSerializer, PupilGroupSerializer, \
    ParentSerlializer, PupilSetSerialiser
from .utils import GroupController


class PupilViewset(viewsets.ViewSet):

    def create(self, request):
        perm = Permissions(request, "admin", request.data['account'])
        recs =Pupil.objects.filter(account=perm.account(), active=True).count()
        if recs >= perm.subscription().allowed_pupils():
            return Response('Max pupil count reached for saidf account', status=status.HTTP_400_BAD_REQUEST)
        ser = PupilSerializer(data=request.data)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        perm = Permissions(request, "auth")
        if 'index' in request.GET:
            recs = Pupil.objects.filter(active=True, account=perm.account(),
                                        last_name__istartswith=request.GET['index'])
        else:
            recs = Pupil.objects.filter(active=True, account=perm.account())
        ser = PupilSerializer(recs, many=True)
        return Response(ser.data)

    def retrieve(self, request, pk):
        perm = Permissions(request, "auth")
        rec = Pupil.objects.filter(account=perm.account(), id=pk, active=True)
        if not rec.exists():
            return Response("Pupil Not found", status=status.HTTP_404_NOT_FOUND)
        ser = PupilSerializer(rec[0])
        return Response(ser.data)

    def destroy(self, request, pk):
        perm = Permissions(request, "admin")
        recs = Pupil.objects.filter(id=pk, account=perm.account())
        for rec in recs:
            rec.active = False
            rec.updated_by = request.user
            rec.save()
        return Response("", status=status.HTTP_204_NO_CONTENT)
        
    def update(self, request, pk):
        perm = Permissions(request, "admin")
        recs = Pupil.objects.filter(id=pk, account=perm.account())
        if not recs.exists():
            return Response('Permission Failed', status=status.HTTP_403_FORBIDDEN)
        rec = recs[0]
        ser = PupilSerializer(rec, data=request.data, partial=True)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True)
    def groups(self, request, pk):
        """
        returns a list og PupilGroup objects  where pk=pupil has membership
        """

        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(active=True, account=perm.account(), groupmemebership__pupil__id=pk)
        ser = PupilGroupSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def can_have_pupil(self, request):
        perm = Permissions(request, "admin")
        count = Pupil.objects.filter(account=perm.account(), active=True).count()
        if perm.subscription().allowed_pupils() >= count:
            return Response({'new_pupils': True, 'pupil_count': count,
                             'pupil_max': perm.subscription().allowed_pupils()})
        else:
            return Response({'new_pupils': True, 'pupil_count': count,
                             'pupil_max': perm.subscription().allowed_pupils()})

    @action(detail=False, methods=['PUT', 'POST'])
    def pupil_import(self,request):
        perm = Permissions(request)
        if not 'file' in request.FILES:
            return Response('No File sent', status=status.HTTP_400_BAD_REQUEST)
        csv_file = csv.DictReader(request.FILES['file'].read().decode('utf-8').splitlines())
        pupils = Pupil.objects.filter(active=True,
                                      account=perm.account()).values_list('email', flat=True)
        new_pupils=[]
        recs = []
        for rec in csv_file:
            recs.append(rec)
            if 'firstname' in rec and 'lastname' in rec and 'email' in rec:
                if not rec['email'] in pupils:
                    new_pupils.append(Pupil(first_name=rec['firstname'],
                                            last_name=rec['lastname'],
                                            email=rec['email'], account=perm.account()),)
        Pupil.objects.bulk_create(new_pupils)
        with transaction.atomic():
            for rec in recs:
                pupil = Pupil.objects.filter(email=rec['email'])
                if pupil.exists():
                    pupil = pupil[0]
                    if 'parent1' in rec:
                        if not pupil.parent_set.filter(email=rec['parent1']).exists():
                            parent = Parent.objects.filter(email=rec['parent1'])
                            if parent.exists():
                                parent = parent[0]
                                parent.pupil.add(pupil)
                                parent.save()
                    if 'parent2' in rec:
                        if not pupil.parent_set.filter(email=rec['parent2']).exists():
                            parent = Parent.objects.filter(email=rec['parent2'])
                            if parent.exists():
                                parent = parent[0]
                                parent.pupil.add(pupil)
                                parent.save()
                    if 'parent3' in rec:
                        if not pupil.parent_set.filter(email=rec['parent3']).exists():
                            parent = Parent.objects.filter(email=rec['parent3'])
                            if parent.exists():
                                parent = parent[0]
                                parent.pupil.add(pupil)
                                parent.save()
                    if 'parent4' in rec:
                        if not pupil.parent_set.filter(email=rec['parent4']).exists():
                            parent = Parent.objects.filter(email=rec['parent4'])
                            if parent.exists():
                                parent = parent[0]
                                parent.pupil.add(pupil)
                                parent.save()
        return Response('File Processed')


class ParentViewset(viewsets.ViewSet):

    def create(self, request):
        perm = Permissions(request, "admin", request.data['account'])
        ser = ParentSerlializer(data=request.data)
        if ser.is_valid():
            if Parent.objects.filter(account=perm.account(), active=True, email=request.data['email']).exists():
                return Response('Parent Record already exists', status=status.HTTP_400_BAD_REQUEST)
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        perm = Permissions(request, "auth")
        if 'index' in request.GET:
            recs = Parent.objects.filter(active=True, account=perm.account(),
                                         last_name__istartswith=request.GET['index'])
        else:
            recs = Parent.objects.filter(account=perm.account(), active=True)
        ser = ParentSerlializer(recs, many=True)
        return Response(ser.data)

    def destroy(self, request, pk):
        perm = Permissions(request, "admin")
        recs = Parent.objects.filter(id=pk, account=perm.account())
        for rec in recs:
            rec.active = False
            rec.updated_by = request.user
            rec.save()
        return Response('', status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk):
        perm = Permissions(request, "admin")
        recs = Parent.objects.filter(id=pk, account=perm.account())
        if not recs.exists():
            return Response('No record Found', status=status.HTTP_400_BAD_REQUEST)
        rec = recs[0]
        ser = ParentSerlializer(rec, data=request.data, partial=True)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True)
    def available(self, request, pk):
        perm = Permissions(request, "admin")
        recs = Parent.objects.filter(active=True, account=perm.account()).exclude(pupil__id=pk)
        ser = ParentSerlializer(recs, many=True)
        return Response(ser.data)

    @action(detail=True)
    def pupils(self, request, pk):
        perm = Permissions(request, "auth")
        recs = Pupil.objects.filter(parent__id=pk, active=True, account=perm.account())
        ser = PupilSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=True)
    def for_pupil(self, request, pk):
        Permissions(request, "auth")
        recs = Parent.objects.filter(active=True, pupil__id=pk)
        ser = ParentSerlializer(recs, many=True)
        return Response(ser.data)

    @action(detail=False)
    def count(self, request):
        Permissions(request, "teacher")
        recs = Parent.objects.filter(active=True).count()
        return Response({'count': recs})

    @action(detail=True,methods=['POST', 'PUT'])
    def add_pupil(self, request, pk):
        Permissions(request, "admin")
        if not 'pupil' in request.data:
            return Response('No pupil id sent', status=status.HTTP_400_BAD_REQUEST)
        rec = Parent.objects.get(id=pk)
        rec.pupil.add(Pupil.objects.get(id=request.data['pupil']))
        rec.updated_by = request.user
        rec.save()
        return Response('Pupil Added')

    @action(detail=True, methods=['POST', 'PUT'])
    def remove_pupil(self, request, pk):
        Permissions(request, "admin")
        if not 'pupil' in request.data:
            return Response('No pupil id sent', status=status.HTTP_400_BAD_REQUEST)
        rec = Parent.objects.get(id=pk)
        rec.pupil.remove(Pupil.objects.get(id=request.data['pupil']))
        rec.updated_by = request.user
        rec.save()
        return Response('Pupil Removed')

    @action(detail=False, methods=['PUT', 'POST'])
    def parent_import(self,request):
        perm = Permissions(request)
        if not 'file' in request.FILES:
            return Response('No File sent', status=status.HTTP_400_BAD_REQUEST)
        csv_file = csv.DictReader(request.FILES['file'].read().decode('utf-8').splitlines())
        parents = Parent.objects.filter(active=True, account=perm.account()).values_list('email', flat=True)
        new_parents=[]
        for rec in csv_file:
            if 'firstname' in rec and 'lastname' in rec and 'email' in rec:
                if not rec['email'] in parents:
                    new_parents.append(Parent(first_name=rec['firstname'],
                                              last_name=rec['lastname'],
                                              email=rec['email'], account=perm.account()),)
        Parent.objects.bulk_create(new_parents)
        return Response('File Processed')


class PupilGroupViewset(viewsets.ViewSet):
    def create(self, request):
        """
        Creates a new Group
        """
        Permissions(request, "admin", request.data['account'])
        ser = PupilGroupSerializer(data=request.data)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        """
        list pupil records
        """
        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(active=True, account=perm.account())
        ser = PupilGroupSerializer(recs, many=True)
        return Response(ser.data)

    def destroy(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, account=perm.account())
        if recs.exists():
            rec = recs[0]
            tree = GroupController(rec, build_tree=True)
            with transaction.atomic():
                for group in tree.all_groups():
                    group.active = False
                    group.updated_by = request.user
                    group.save()
        return Response('Deleted', status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk):
        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(id=pk, account=perm.account())
        if not recs.exists():
            return Response("Records not found for your account", status=status.HTTP_403_FORBIDDEN)
        ser = PupilGroupSerializer(recs[0])
        return Response(ser.data)

    def update(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, account=perm.account())
        if not recs.exists():
            return Response('Record not found', status=status.HTTP_400_BAD_REQUEST)
        ser = PupilGroupSerializer(recs[0], data=request.data, partial=True)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True)
    def list_membership(self, request, pk):
        perm = Permissions(request, "auth")
        rec = GroupController(PupilGroup.objects.get(id=pk, account=perm.account()))
        ser = PupilSerializer(Pupil.objects.filter(id__in=rec.list_membership()), many=True)
        return Response(ser.data)

    @action(detail=True)
    def list_not_in_group(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, active=True, account=perm.account())
        if not recs.exists():
            return Response('Group Not found', status=status.HTTP_400_BAD_REQUEST)
        group = GroupController(recs[0])
        pupil_recs = Pupil.objects.filter(id__in=group.list_possible_membership())
        ser = PupilSerializer(pupil_recs, many=True)
        return Response(ser.data)

    @action(detail=True)
    def clear_group(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, active=True, account=perm.account())
        if not recs.exists():
            return Response('Group Not found', status=status.HTTP_400_BAD_REQUEST)
        group = GroupController(recs[0])
        group.clear_group(request)
        return Response("Groups cleared", status=status.HTTP_204_NO_CONTENT)

    @action(detail=False)
    def list_no_parent(self, request):
        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(parent__isnull=True, account=perm.account())
        ser = PupilGroupSerializer(recs, many=True)
        return Response(ser.data)

    @action(detail=True)
    def list_subgroups(self, request, pk):
        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(id=pk, active=True, account=perm.account())
        if not recs.exists():
            return Response('Group Not found', status=status.HTTP_400_BAD_REQUEST)
        group = GroupController(recs[0])
        ser = PupilGroupSerializer(group.list_subgroups(), many=True)
        return Response(ser.data)

    @action(detail=True)
    def list_subgroup_membership(self, request, pk):
        perm = Permissions(request, "auth")
        recs = PupilGroup.objects.filter(id=pk, active=True, account=perm.account())
        if not recs.exists():
            return Response('Group Not found', status=status.HTTP_400_BAD_REQUEST)
        group = GroupController(recs[0])
        data = group.list_subgroup_membership()
        return Response(data)

    @action(detail=True)
    def move_membership(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, active=True, account=perm.account())
        if not recs.exists():
            return Response('Group Not found', status=status.HTTP_400_BAD_REQUEST)
        new_recs = PupilGroup.objects.filter(id=request.data['new_group'], account=perm.account())
        if not new_recs.exists():
            return Response('New Group does not exist', status=status.HTTP_400_BAD_REQUEST)
        group = GroupController(recs[0])
        res = group.move_group(new_recs[0], request)
        if res:
            return Response('Moved OK', status=status.HTTP_200_OK)
        else:
            return Response('Moved Failed, Please call System Admin', status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False)
    def free(self, request):
        perm = Permissions(request, "admin")
        if PupilGroup.objects.filter(account=perm.account(),
                                     name=request.GET['name'], active=True).exists():
            return Response('Already exists', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('free')

    @action(detail=False, methods=['PUT', 'POST'])
    def group_import(self,request):
        perm = Permissions(request)
        if not 'file' in request.FILES:
            return Response('No File sent', status=status.HTTP_400_BAD_REQUEST)
        csv_file = csv.DictReader(request.FILES['file'].read().decode('utf-8').splitlines())
        groups = PupilGroup.objects.filter(active=True,
                                           account=perm.account()).values_list('name', flat=True)
        new_groups=[]
        recs = []
        for rec in csv_file:
            recs.append(rec)
            if 'group' in rec:
                if not rec['group'] in groups:
                    new_groups.append(PupilGroup(name=rec['group'], account=perm.account()),)
        PupilGroup.objects.bulk_create(new_groups)
        with transaction.atomic():
            for rec in recs:
                if 'parent' in rec:
                    group_rec = PupilGroup.objects.filter(active=True, name=rec['group'])
                    if group_rec.exists():
                        group_rec = group_rec[0]
                        parent_rec = PupilGroup.objects.filter(active=True, name=rec['parent'])
                        if parent_rec.exists():
                            parent_rec = parent_rec[0]
                            group_rec.parent = parent_rec
                            group_rec.save()
        return Response('file Processed')

    @action(detail=True)
    def list_all_subgroups(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilGroup.objects.filter(id=pk, account=perm.account())
        if recs.exists():
            rec = recs[0]
            tree = GroupController(rec, build_tree=True)
            ser = PupilGroupSerializer(tree.all_groups(), many=True)
            return Response(ser.data)
        return Response([])


class PupilSetsViewset(viewsets.ViewSet):

    def create(self, request):
        perm =Permissions(request, "admin", request.data['account'])
        if PupilSets.objects.filter(name=request.data['name'],
                                    active=True, account=perm.account()).exists():
            return Response('PupilSet already exists', status=status.HTTP_400_BAD_REQUEST)
        ser = PupilSetSerialiser(data=request.data)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return (ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        perm = Permissions(request, "admin")
        recs = PupilSets.objects.filter(id=pk, account=perm.account(), active=True)
        if not recs.exists():
            return Response('Record Not found', status=status.HTTP_404_NOT_FOUND)
        rec = recs[0]
        ser = PupilSetSerialiser(rec, data=request.data, partial=True)
        if ser.is_valid():
            ser.save(updated_by=request.user)
            return Response(ser.data)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        perm = Permissions(request, 'auth')
        recs = PupilSets.objects.filter(active=True, account=perm.account())
        ser = PupilSetSerialiser(recs, many=True)
        return Response(ser.data)




class GroupMembershipViewset(viewsets.ViewSet):

    def create(self, request):
        perm = Permissions(request, "admin")
        pupil = Pupil.objects.get(id=request.data['pupil'], account=perm.account())
        group = GroupController(PupilGroup.objects.get(id=request.data['group'], active=True))
        group.add_member(pupil, request)
        return Response('Added to groups')

    @action(detail=False, methods=['POST'])
    def remove_member(self, request):
        perm = Permissions(request, "admin")
        pupil = Pupil.objects.get(id=request.data['pupil'], account=perm.account())
        group = GroupController(PupilGroup.objects.get(id=request.data['group'], active=True))
        group.remove_member(pupil, request)
        return Response('Removed to groups')

    @action(detail=False, methods=['PUT', 'POST'])
    def membership_import(self,request):
        perm = Permissions(request)
        if not 'file' in request.FILES:
            return Response('No File sent', status=status.HTTP_400_BAD_REQUEST)
        csv_file = csv.DictReader(request.FILES['file'].read().decode('utf-8').splitlines())
        groups = PupilGroup.objects.filter(active=True,id=request.data['group'], account=perm.account())
        if not groups.exists():
            return Response('Group not found', status=status.HTTP_400_BAD_REQUEST)
        group = groups[0]
        with transaction.atomic():
            for rec in csv_file:
                pupil_rec = Pupil.objects.get(email=rec['pupil_email'], account=perm.account())
                if not GroupMembership.objects.filter(group=group, pupil=pupil_rec).exists():
                    GroupMembership.objects.create(group=group, pupil=pupil_rec)
        return Response('file Processed')