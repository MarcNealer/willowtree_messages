from rest_framework import routers
from .views import PupilViewset, PupilGroupViewset, GroupMembershipViewset
from .views import ParentViewset, PupilSetsViewset


router = routers.DefaultRouter()

router.register(r'pupils', PupilViewset, base_name='pupils')
router.register(r'pupilgroups', PupilGroupViewset, base_name='pupil_groups')
router.register(r'pupilsets', PupilSetsViewset, base_name='pupil_sets')
router.register(r'membership', GroupMembershipViewset, base_name='group_membership')
router.register(r'parents', ParentViewset, base_name="parents")

urlpatterns = [] + router.urls