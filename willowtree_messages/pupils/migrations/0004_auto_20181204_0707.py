# Generated by Django 2.0.7 on 2018-12-04 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pupils', '0003_pupil_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parent',
            name='pupil',
            field=models.ManyToManyField(blank=True, to='pupils.Pupil'),
        ),
    ]
