from django.contrib import admin
from .models import Pupil, PupilGroup, GroupMembership, Parent, PupilSets
# Register your models here.

class ParentAdmin(admin.ModelAdmin):
    filter_horizontal = ('pupil',)

admin.site.register(Pupil)
admin.site.register(PupilGroup)
admin.site.register(PupilSets)
admin.site.register(GroupMembership)
admin.site.register(Parent, ParentAdmin)

