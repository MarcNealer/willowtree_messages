from rest_framework import serializers
from .models import Pupil, PupilGroup, PupilSets, Parent, GroupMembership


class PupilSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pupil
        fields = "__all__"

    parent_count = serializers.SerializerMethodField()

    def get_parent_count(self, obj):
        return obj.parent_set.filter(active=True).count()


class PupilGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = PupilGroup
        fields = "__all__"

    membership_count = serializers.SerializerMethodField()

    def get_membership_count(self, obj):
        return len(set(GroupMembership.objects.filter(active=True, group=obj).values_list('pupil__id')))


class PupilSetSerialiser(serializers.ModelSerializer):

    class Meta:
        model = PupilSets
        fields = "__all__"


class ParentSerlializer(serializers.ModelSerializer):

    class Meta:
        model = Parent
        fields = "__all__"
    pupil_count = serializers.SerializerMethodField()

    def get_pupil_count(self, obj):
        return obj.pupil.filter(active=True).count()
