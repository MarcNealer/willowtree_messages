from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from accounts.serializers import AccountsSerializer
from django.contrib.auth import login
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string
# Create your views here.

def LoginPage(request):
    return render(request, 'login_page.html')

@login_required()
def MainPage(request):
    return render(request, 'main_page.html')

def LandingPage(request):
    return render(request, 'landing.html')

def Terms(request):
    return render(request, 'terms.html')

def DataProtection(request):
    return render(request, 'dataprotection.html')
